﻿using System;
using System.Drawing;
using System.Dynamic;
using System.Linq;
using System.Threading;
using Newtonsoft.Json;
using OpenQA.Selenium.PhantomJS;
using RestSharp;
using RestSharp.Serializers;
using Scraper.Helpers;
using Scraper.Model;

namespace Scraper
{
    internal class Program
    {
        private static void Main()
        {
            PhantomJSDriver driver = null;

            try
            {
                var options = ScraperFactory.CreateDriverOptions();
                var service = ScraperFactory.CreateDriverService();
                driver = new PhantomJSDriver(service, options);

                while (true)
                {
                    var task = GetTaskFromTaskPool();

                    if (task == null || task.Id == Guid.Empty)
                    {
                        Thread.Sleep(30 * 1000);
                    }
                    else
                    {
                        driver.Manage().Window.Size =
                            new Size(1920, 1080); // Size is a type in assembly "System.Drawing"
                        driver.Navigate().GoToUrl("https://www.njuskalo.hr/");

                        driver.WaitForDriverLoad();

                        var scraper = new NjuskaloScraper(driver);
                        var categories = scraper.ExtractCategories();

                        task.AdditionalData = JsonConvert.SerializeObject(categories);
                        task.Status = "Finished";

                        Console.WriteLine("Updating status Task {0}", task.TaskName);

                        var response = UpdateTaskStatus(task);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.ReadLine();
            }
            finally
            {
                driver?.Dispose();
            }
        }

        public static Task UpdateTaskStatus(Task task)
        {
            var client = new RestClient("http://localhost:50391/api/");
            var request = new RestRequest("Task", Method.POST);
            request.RequestFormat = DataFormat.Json;

            request.AddBody(new
            {
                Id = task.Id,
                TaskName = task.TaskName,
                TaskDescription = task.TaskDescription,
                Status = task.Status,
                AdditionalData = task.AdditionalData
            });

            var taskRes = client.Execute<Task>(request);

            return taskRes.Data;
        }

        public static Task GetTaskFromTaskPool()
        {
            Console.WriteLine("Getting Task From Work Pool");

            var client = new RestClient("http://localhost:50391/api/");
            var request = new RestRequest("Task/WorkPool", Method.GET);

            var taskRes = client.Execute<Task>(request);

            Console.WriteLine("Task: {0}", taskRes?.Data?.TaskName);
            return taskRes?.Data;
        }
    }
}