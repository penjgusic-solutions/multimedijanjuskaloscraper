﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Scraper.Helpers
{
    public static class DriverExtensions
    {
        public static void WaitForDriverLoad(this IWebDriver driver)
        {
            IWait<IWebDriver> wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30.00));
            wait.Until(driver1 => ((IJavaScriptExecutor)driver).ExecuteScript("return document.readyState").Equals("complete"));
        }
    }
}