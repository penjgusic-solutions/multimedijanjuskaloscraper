﻿namespace Scraper.Model
{
    public class Rubric
    {
        public string RubricName { get; set; }

        public string RubricUrl { get; set; }
    }
}