﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scraper.Model
{
    public class Category
    {
        public string CategoryName { get; set; }

        public string CategoryUrl { get; set; }

        public List<SubCategory> SubCategories { get; set; }
    }
}
