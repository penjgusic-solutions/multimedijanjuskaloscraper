﻿using System;
using System.Collections.Generic;

namespace Scraper.Model
{
    public class Tasks
    {
        public List<Task> TaskList { get; set; }
    }

    public class Task
    {
        public Guid Id { get; set; }

        public string TaskName { get; set; }

        public string TaskDescription { get; set; }

        public string Status { get; set; }

        public string AdditionalData { get; set; }
    }
}