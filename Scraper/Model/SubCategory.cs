﻿using System.Collections.Generic;

namespace Scraper.Model
{
    public class SubCategory
    {
        public string SubCategoryName { get; set; }

        public string SubCategoryUrl { get; set; }

        public List<Rubric> Rubrics { get; set; }
    }
}