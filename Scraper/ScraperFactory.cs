﻿using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;

namespace Scraper
{
    public static class ScraperFactory
    {
        public static PhantomJSDriverService CreateDriverService()
        {
            var driverService = PhantomJSDriverService.CreateDefaultService();
            driverService.HideCommandPromptWindow = false;
            driverService.LoadImages = false;
            driverService.ProxyType = "none";
            driverService.SslProtocol = "any";

            return driverService;

        }

        public static PhantomJSOptions CreateDriverOptions()
        {
            PhantomJSOptions options = new PhantomJSOptions();
            options.AddAdditionalCapability("phantomjs.page.settings.userAgent", "Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25");
            return options;
        }
    }
}