﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Imaging;
using System.Xml.XPath;
using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using Scraper.Helpers;
using Scraper.Model;

namespace Scraper
{
    public class NjuskaloScraper
    {
        private readonly PhantomJSDriver _driver;

        public NjuskaloScraper(PhantomJSDriver driver)
        {
            _driver = driver;
        }

        public List<Category> ExtractCategories()
        {
            var mainSite = ConfigurationManager.AppSettings["njuskalo"] as string;
            var map = ConfigurationManager.AppSettings["url"] as string;

            _driver.Navigate().GoToUrl("https://www.njuskalo.hr/sitemap/");

            _driver.WaitForDriverLoad();

            _driver.GetScreenshot().SaveAsFile(@"c:\Users\Jurica\Desktop\phantomjs1_screenshot.png", ImageFormat.Jpeg);

            var pageSource = _driver.PageSource;

            if (string.IsNullOrEmpty(pageSource))
            {
                throw new NullReferenceException("Can not  load page source");
            }

            var doc = new HtmlDocument();
            doc.LoadHtml(pageSource);

            HtmlNode rootNode = doc.DocumentNode.SelectSingleNode("//div[contains(@class, 'passage-standard')]/ul[2]");

            var categoryList = new List<Category>();

            foreach (var node in rootNode.ChildNodes)
            {
                if (node.Name == "li")
                {
                    var currentNode = node?.SelectSingleNode(".//a");
                    var name = currentNode?.InnerText;
                    var catUrl = currentNode?.Attributes["href"]?.Value;

                    var category = new Category
                    {
                        CategoryName = name,
                        CategoryUrl = catUrl,
                    };

                    if (node.NextSibling?.NextSibling != null && node.NextSibling.NextSibling.Name == "ul")
                    {
                        var subs = ExtractSubCategories(node.NextSibling.NextSibling);
                        category.SubCategories = subs;
                    }

                    categoryList.Add(category);
                }
            }

            return categoryList;
        }

        private List<SubCategory> ExtractSubCategories(HtmlNode nextSibling)
        {
            var listOfSubs = new List<SubCategory>();

            if (nextSibling.ChildNodes == null) return listOfSubs;

            foreach (var node in nextSibling.ChildNodes)
            {
                if (node.Name == "li")
                {
                    var currentNode = node.SelectSingleNode(".//a");
                    var name = currentNode.InnerText;
                    var catUrl = currentNode.Attributes["href"]?.Value;

                    var subCategory = new SubCategory
                    {
                        SubCategoryName = name,
                        SubCategoryUrl = catUrl,
                    };

                    if (node.NextSibling?.NextSibling != null && node.NextSibling.NextSibling.Name == "ul")
                    {
                        var rubics = ExtractRubics(node.NextSibling.NextSibling);
                        subCategory.Rubrics = rubics;
                    }

                    listOfSubs.Add(subCategory);
                }
            }
            return listOfSubs;
        }

        private List<Rubric> ExtractRubics(HtmlNode nodeNextSibling)
        {
            var listOfRubric = new List<Rubric>();

            if (nodeNextSibling.ChildNodes == null) return listOfRubric;

            foreach (var node in nodeNextSibling.ChildNodes)
            {
                if (node.Name == "li")
                {
                    var currentNode = node?.SelectSingleNode(".//a");
                    var name = currentNode?.InnerText;
                    var catUrl = currentNode?.Attributes["href"]?.Value;

                    var rubric = new Rubric
                    {
                        RubricName = name,
                        RubricUrl = catUrl,
                    };

                    listOfRubric.Add(rubric);
                }
            }

            return listOfRubric;
        }
    }
}