﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Scraper.Web.InMemoryDB;
using Scraper.Web.Models;

namespace Scraper.Web.Controllers
{
    public class TasksController : Controller
    {
        private readonly Tasks _tasks = Tasks.Instance;

        // GET: Tasks
        public ActionResult Index()
        {
            return View(_tasks.TaskList.ToList());
        }

        // GET: Tasks/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Task task = _tasks.TaskList.Find(x => x.Id == id);
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(task);
        }

        // GET: Tasks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tasks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TaskName,TaskDescription")] Task task)
        {
            if (ModelState.IsValid)
            {
                task.Id = Guid.NewGuid();
                task.Status = "Pending";
                 
                _tasks.TaskList.Add(task);

                return RedirectToAction("Index");
            }

            return View(task);
        }

        // GET: Tasks/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Task task = _tasks.TaskList.Find(x => x.Id == id);
            if (task == null)
            {
                return HttpNotFound();
            }
            return View(task);
        }

        // POST: Tasks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            Task task = _tasks.TaskList.Find(x => x.Id == id);
            _tasks.TaskList.Remove(task);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            base.Dispose(disposing);
        }
    }
}
