﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Scraper.Web.Startup))]
namespace Scraper.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
