﻿using System.Linq;
using System.Web.Http;
using Scraper.Web.InMemoryDB;

namespace Scraper.Web.Api
{
    public class TaskController : ApiController
    {
        private readonly Tasks _tasks = Tasks.Instance;

        // GET: api/Task
        public Task Get()
        {
            return _tasks.TaskList.FirstOrDefault(x => x.Status == "Pending");
        }

        // POST: api/Task
        public IHttpActionResult Post([FromBody] Task task)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var selected = _tasks.TaskList.FirstOrDefault(x => x.Id == task.Id);

            if (selected == null)
                return BadRequest("There is no such task");

            selected.Status = "Finished";
            selected.AdditionalData = task.AdditionalData;

            return Ok();
        }

        // PUT: api/Task/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/Task/5
        public void Delete(int id)
        {
        }
    }
}