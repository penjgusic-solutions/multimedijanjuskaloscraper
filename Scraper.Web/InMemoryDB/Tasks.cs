﻿using System;
using System.Collections.Generic;

namespace Scraper.Web.InMemoryDB
{
    public class Tasks
    {
        private static Tasks _instance;

        private Tasks()
        {
            TaskList = new List<Task>();
        }

        public static Tasks Instance => _instance ?? (_instance = new Tasks());

        public List<Task> TaskList { get; set; }
    }

    public class Task
    {
        public Guid Id { get; set; }

        public string TaskName { get; set; }

        public string TaskDescription { get; set; }

        public string Status { get; set; }

        public string AdditionalData { get; set; }
    }
}